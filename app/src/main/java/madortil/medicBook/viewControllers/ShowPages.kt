package madortil.medicBook.viewControllers

import android.content.Intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_show_pages.*
import madortil.medicBook.CategoryAndPageFIreManipulator
import madortil.medicBook.DataHolder
import madortil.medicBook.R
import madortil.medicBook.models.HourPeriod
import madortil.medicBook.models.hourPeriod

class ShowPages : AppCompatActivity() {

    private val _pageNamesArray = arrayOfNulls<String>(DataHolder.categories!![DataHolder.currentCategory].pages.size)
    private var pagePos = 0
    private var manipulator: CategoryAndPageFIreManipulator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_pages)
        manipulator = CategoryAndPageFIreManipulator(baseContext)
        setPageNamesArray()
        setUpUI()
        title = DataHolder.categories!![DataHolder.currentCategory].name
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (DataHolder.isAdmin) {
            val inflater = MenuInflater(applicationContext)
            inflater.inflate(R.menu.add_new_category_and_page, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        progressbar_deletePage.visibility = View.VISIBLE
        val categoryKey = DataHolder.categories!![DataHolder.currentCategory].key
        manipulator!!.createNewPage(categoryKey, this)

        return super.onOptionsItemSelected(item)
    }

    private fun setUpUI() {
        categoryThemeImageView.setBackgroundResource(
                when (hourPeriod) {
                    HourPeriod.Morning -> R.drawable.ic_screen_background_morningr
                    HourPeriod.Noon -> R.drawable.ic_screen_background_noonr
                    else -> R.drawable.ic_screen_background_nightr
                })
        categoryThemeImageView.setImageResource(categoryThemeGlowImage(DataHolder.currentCategory))

        val adapter = ArrayAdapter<String>(this, R.layout.page_cell, _pageNamesArray)
        listview_pages.adapter = adapter
        DataHolder.adaptersToUpdate.add(adapter)
        listview_pages.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ -> onPageClickDisplayPDF(position) }
        listview_pages.onItemLongClickListener = AdapterView.OnItemLongClickListener { _, _, position, _ ->
            pagePos = position
            false
        }
        registerForContextMenu(listview_pages)
    }
    private fun categoryThemeGlowImage(byInt: Int): Int {
        return when(byInt%10) {
            0 -> R.drawable.ic_category_theme_glow_1
            1 -> R.drawable.ic_category_theme_glow_2
            2 -> R.drawable.ic_category_theme_glow_3
            3 -> R.drawable.ic_category_theme_glow_4
            4 -> R.drawable.ic_category_theme_glow_5
            5 -> R.drawable.ic_category_theme_glow_6
            6 -> R.drawable.ic_category_theme_glow_7
            7 -> R.drawable.ic_category_theme_glow_8
            8 -> R.drawable.ic_category_theme_glow_9
            else -> R.drawable.ic_category_theme_glow_10
        }
    }

    private fun onPageClickDisplayPDF(pos: Int) {
        DataHolder.currentPage = pos
        val displayPDFPage = Intent(baseContext, DisplayPDFFiles::class.java)
        startActivity(displayPDFPage)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        DataHolder.updateAdapters()
        val intent = intent
        finish()
        startActivity(intent)
    }

    private fun setPageNamesArray() {
        val currentCat = DataHolder.currentCategory
        val pages = DataHolder.categories!![currentCat].pages
        pages.sortBy { it.name }
        for (i in pages.indices) {
            _pageNamesArray[i] = pages[i].name
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo) {
        if (DataHolder.isAdmin) {
            super.onCreateContextMenu(menu, v, menuInfo)
            val inflater = MenuInflater(applicationContext)
            inflater.inflate(R.menu.edit_page, menu)
        }
    }

    //TODO add loading indicator and disable ui when deleting
    override fun onContextItemSelected(item: MenuItem): Boolean {
        progressbar_deletePage.visibility = View.VISIBLE
        if (item.itemId == R.id.editPage) {
            val intent = Intent(applicationContext, ChangeCategoryAndPageNames::class.java)
            intent.putExtra("name", _pageNamesArray[pagePos])
            intent.putExtra("type", DataHolder.ItemType.PAGE)
            startActivityForResult(intent, 0)
        } else {
            manipulator!!.deletePage(this, DataHolder.currentPage, DataHolder.categories!![DataHolder.currentCategory])
        }
        return super.onContextItemSelected(item)
    }
}
