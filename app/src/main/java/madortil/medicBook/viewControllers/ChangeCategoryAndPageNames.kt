package madortil.medicBook.viewControllers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_change_category_and_page_names.*
import madortil.medicBook.DataHolder
import madortil.medicBook.R
import madortil.medicBook.models.HourPeriod
import madortil.medicBook.models.hourPeriod
import madortil.medicBook.models.keys

import java.util.HashMap

class ChangeCategoryAndPageNames : AppCompatActivity() {

    internal var type: DataHolder.ItemType = DataHolder.ItemType.NO_ID

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_category_and_page_names)
        setUpUIElements()
        getExtra()
        title = "שנה שם"
    }

    private fun getExtra() {
        currentName.text = intent.getStringExtra("name")
        this.type = intent.getSerializableExtra("type") as DataHolder.ItemType
    }

    private fun setUpUIElements() {
        backgroundImageView.setImageResource(
                when (hourPeriod) {
                    HourPeriod.Morning -> R.drawable.ic_screen_background_morningr
                    HourPeriod.Noon -> R.drawable.ic_screen_background_noonr
                    else -> R.drawable.ic_screen_background_nightr
                })
        confirmChange.setOnClickListener { confirmButtonOnClick() }
    }

    private fun disableActivity(status: Boolean) {
        if (status) {
            progressbar_changeName.visibility = View.VISIBLE
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            progressbar_changeName.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    private fun confirmButtonOnClick() {
        disableActivity(true)
        val catKey = DataHolder.categories!![DataHolder.currentCategory].key
        val pgeKey = DataHolder.categories!![DataHolder.currentCategory].pages[DataHolder.currentPage].pageKey

        if (type == DataHolder.ItemType.CATEGORY) {
            val childValues = HashMap<String, Any>()
            childValues["name"] = editName.text.toString()
            FirebaseDatabase.getInstance().reference.child(keys.BOOK_GROUP).child(catKey).updateChildren(childValues).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(baseContext, "שונה בהצלחה", Toast.LENGTH_SHORT).show()
                    disableActivity(false)
                    setResult(0)
                    finish()
                }
            }
        } else if (type == DataHolder.ItemType.PAGE) {
            val updatedValues = HashMap<String, Any>()
            updatedValues["name"] = editName.text.toString()
            FirebaseDatabase.getInstance().reference.child(keys.BOOK_GROUP).child(catKey).child(keys.PAGES).child(pgeKey).updateChildren(updatedValues).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(baseContext, "שונה בהצלחה", Toast.LENGTH_SHORT).show()
                    setResult(0)
                    finish()
                }
            }
        }
    }
}
