package madortil.medicBook.viewControllers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_admin_login.*
import madortil.medicBook.DataHolder
import madortil.medicBook.R
import madortil.medicBook.models.HourPeriod
import madortil.medicBook.models.keys
import java.util.*
import madortil.medicBook.models.hourPeriod

class AdminLogin : AppCompatActivity() {

    internal lateinit var pass: TextView
    private lateinit var checkpass: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_login)
        setUpUI()
    }

    private fun setUpUI() {
        backgroundImageView.setImageResource(
                when (hourPeriod) {
                    HourPeriod.Morning -> R.drawable.ic_screen_background_morningr
                    HourPeriod.Noon -> R.drawable.ic_screen_background_noonr
                    else -> R.drawable.ic_screen_background_nightr
                })
        passbutton.setOnClickListener {
            FirebaseDatabase.getInstance().reference.child(keys.PASSCODE).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val currentPass = passtext.text.toString()
                    val realPass = dataSnapshot.value!!.toString()
                    if (currentPass == realPass) {
                        DataHolder.isAdmin = true
                        Toast.makeText(applicationContext, "אתה מנהל!", Toast.LENGTH_SHORT).show()
                        continueToShowCategories()
                    } else {
                        Toast.makeText(baseContext, "אופס! הסיסמא לא נכונה", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Toast.makeText(applicationContext, "קרתה שגיאה, נסה שנית", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    private fun continueToShowCategories() {
        val intent = Intent(applicationContext, ShowCategories::class.java)
        startActivity(intent)
    }
}
