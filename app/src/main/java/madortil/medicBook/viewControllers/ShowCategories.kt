package madortil.medicBook.viewControllers

import android.content.Intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_show_categories.*
import madortil.medicBook.CategoryAndPageFIreManipulator
import madortil.medicBook.DataHolder
import madortil.medicBook.R
import madortil.medicBook.models.HourPeriod
import madortil.medicBook.models.hourPeriod

class ShowCategories : AppCompatActivity() {

    private val _categoryStringArray = arrayOfNulls<String>(DataHolder.categories!!.size)
    private var currentLongClickItemPos = 0
    private var manipulator: CategoryAndPageFIreManipulator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_categories)
        manipulator = CategoryAndPageFIreManipulator(baseContext)
        setCategoryStringArray()
        setUpUIElements()
        title = "מדיקבוק"
    }


    private fun setUpUIElements() {
        listview_categories.setBackgroundResource(
                when (hourPeriod) {
                    HourPeriod.Morning -> R.drawable.ic_screen_background_morningr
                    HourPeriod.Noon -> R.drawable.ic_screen_background_noonr
                    else -> R.drawable.ic_screen_background_nightr
                })
        val adapter = ArrayAdapter<String>(this, R.layout.category_cell, _categoryStringArray)
        listview_categories.adapter = adapter
        DataHolder.adaptersToUpdate.add(adapter)
        listview_categories.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ -> onCategoryClick(position) }
        listview_categories.onItemLongClickListener = AdapterView.OnItemLongClickListener { _, _, position, _ ->
            currentLongClickItemPos = position
            DataHolder.currentCategory = position
            false
        }
        registerForContextMenu(listview_categories)

    }

    private fun onCategoryClick(pos: Int) {
        DataHolder.currentCategory = pos
        val viewAsocPagesIntent = Intent(baseContext, ShowPages::class.java)
        startActivity(viewAsocPagesIntent)
    }

    private fun setCategoryStringArray() {
        for (i in _categoryStringArray.indices) {
            _categoryStringArray[i] = DataHolder.categories!![i].name
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo) {
        if (DataHolder.isAdmin) {
            super.onCreateContextMenu(menu, v, menuInfo)
            val inflater = menuInflater
            inflater.inflate(R.menu.edit_page, menu)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (DataHolder.isAdmin) {
            val inflater = MenuInflater(baseContext)
            inflater.inflate(R.menu.add_new_category_and_page, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    //TODO check why creating a category prints a mistake
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        progressbar_catLoading.visibility = View.VISIBLE
        manipulator!!.createCategory(this)
        return super.onOptionsItemSelected(item)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        progressbar_catLoading.visibility = View.VISIBLE
        if (R.id.editPage == item.itemId) {
            startEdit()
        } else {
            manipulator!!.deleteCategory(this, DataHolder.categories!![DataHolder.currentCategory])
        }
        return super.onContextItemSelected(item)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        DataHolder.updateAdapters()
        val intent = intent
        finish()
        startActivity(intent)
    }

    private fun startEdit() {
        val intent = Intent(applicationContext, ChangeCategoryAndPageNames::class.java)
        intent.putExtra("name", _categoryStringArray[currentLongClickItemPos])
        intent.putExtra("type", DataHolder.ItemType.CATEGORY)
        startActivityForResult(intent, 0)
    }

    private fun ShowText(Text: String) {
        Toast.makeText(baseContext, Text, Toast.LENGTH_SHORT).show()
    }


}
