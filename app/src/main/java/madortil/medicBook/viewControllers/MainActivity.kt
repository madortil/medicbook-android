package madortil.medicBook.viewControllers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import madortil.medicBook.DataHolder
import madortil.medicBook.R
import madortil.medicBook.models.HourPeriod
import madortil.medicBook.models.category
import madortil.medicBook.models.hourPeriod
import madortil.medicBook.models.keys

import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUIElements()
        setUIElementsListeners()
        anonymousLogin()

        title = "מדיקבוק"
    }

    private fun setUIElements() {
        backgroundImageView.setImageResource(
                when (hourPeriod) {
                    HourPeriod.Morning -> R.drawable.ic_screen_background_morningm
                    HourPeriod.Noon -> R.drawable.ic_screen_background_noonm
                    else -> R.drawable.ic_screen_background_nightm
                })
        textviewCred.text = "מהנדס תוכנה משני - דן פכטמן \n מדור ט׳יל, בה׳ד 10"
    }

    private fun setUIElementsListeners() {
        adminLogin.setOnClickListener {
            val intent = Intent(applicationContext, AdminLogin::class.java)
            startActivity(intent)
        }
        userLogin.setOnClickListener {
            val intent = Intent(applicationContext, ShowCategories::class.java)
            startActivity(intent)
        }
    }

    private fun changeControlInteractability(status: Boolean?) {
        adminLogin.isEnabled = status!!
        userLogin.isEnabled = status
    }

    private fun setButtonOpacity(opacity: Float) {
        adminLogin.alpha = opacity
        userLogin.alpha = opacity
    }

    private fun setButtonInteraction(status: Boolean) {
        //interaction enabled
        if (status) {
            textview_loadingText.visibility = View.INVISIBLE
            readingDatabaseProgress.visibility = View.INVISIBLE
            changeControlInteractability(status)
            setButtonOpacity(1.0f)
        } else {
            textview_loadingText.visibility = View.VISIBLE
            readingDatabaseProgress.visibility = View.VISIBLE
            changeControlInteractability(status)
            setButtonOpacity(0.5f)
        }//interaction disabled
    }

    private fun readBooksAndPages() {
        DataHolder.categories = ArrayList()
        FirebaseDatabase.getInstance().reference.child(keys.BOOK_GROUP).addValueEventListener(object : ValueEventListener {
            var idCounter = 0
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                DataHolder.categories!!.clear()
                for (snap in dataSnapshot.children) {
                    val catKey = snap.key
                    val catName = snap.child(keys.NAME).value!!.toString()
                    val newCat = category(catKey!!, catName, idCounter)

                    for (page in snap.child(keys.PAGES).children) {
                        idCounter++
                        val pgeName = page.child(keys.NAME).value!!.toString()
                        val pgeURL = page.child(keys.URL).value!!.toString()
                        val pgeKey = page.key!!.toString()
                        newCat.pages.add(madortil.medicBook.models.page(pgeName, pgeURL, idCounter, pgeKey, catKey))
                    }
                    idCounter++
                    DataHolder.categories!!.add(newCat)
                }
                DataHolder.categories!!.sortBy { it.name }
                for (index in DataHolder.categories!!.indices) {
                    DataHolder.categories!![index].pages.sortBy { it.name }
                }

                setButtonInteraction(true)
                DataHolder.updateAdapters()
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    private fun showText(text: String) {
        Toast.makeText(baseContext, text, Toast.LENGTH_SHORT).show()
    }

    private fun anonymousLogin() {
        setButtonInteraction(false)
        val auth = FirebaseAuth.getInstance()
        auth.signInAnonymously().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.d("result", "success")
                readBooksAndPages()
            } else {
                Log.d("result", "isn't successful")
                setButtonInteraction(true)
            }
        }
    }
}
